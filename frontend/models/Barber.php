<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%barber}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $yearsOFservice
 * @property string $phone
 * @property integer $status
 *
 * @property Appointment[] $appointments
 */
class Barber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%barber}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'yearsOFservice', 'phone', 'status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 40],
            [['yearsOFservice', 'phone'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'yearsOFservice' => 'Years Of service',
            'phone' => 'Phone',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments()
    {
        return $this->hasMany(Appointment::className(), ['barberid' => 'id']);
    }
}

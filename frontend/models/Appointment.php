<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appointment}}".
 *
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property string $dateSTARTfrom
 * @property string $dateENDfrom
 * @property integer $serviceid
 * @property integer $customerid
 * @property integer $barberid
 *
 * @property Customer $customer
 * @property Barber $barber
 * @property Service $service
 */
class Appointment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appointment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time',  'serviceid', 'customerid', 'barberid'], 'required'],
            [['serviceid', 'customerid', 'barberid'], 'integer'],
            [['date'], 'string', 'max' => 100],
            [['time'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'time' => 'Time',
            'serviceid' => 'Service',
            'customerid' => 'Customer',
            'barberid' => 'Barber',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customerid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarber()
    {
        return $this->hasOne(Barber::className(), ['id' => 'barberid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'serviceid']);
    }
}

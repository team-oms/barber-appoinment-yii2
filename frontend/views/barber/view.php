<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Barber */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Barbers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barber-view">

    <h1>Barber Name:<?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            #'id',
            'name',
            'yearsOFservice',
            'phone',
            #'status',
        ],
    ]) ?>

</div>

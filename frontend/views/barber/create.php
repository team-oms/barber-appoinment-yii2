<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Barber */

$this->title = 'Create Barber';
$this->params['breadcrumbs'][] = ['label' => 'Barbers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barber-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use app\models\Service;
use app\models\Customer;
use app\models\Barber;
use yii\helpers\ArrayHelper;




/* @var $this yii\web\View */
/* @var $model app\models\Appointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
	
    <div class="col-md-6">
		<div class="panel panel-default">
		<div class="panel-body">



    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true]) 
    ->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter the date ...'],
    'pluginOptions' => [
        'autoclose'=>true,
		'startDate'=>date('M-d-Y'),
		'format' => 'dd-M-yyyy'
    ]
]);
?>


 <?php echo '<label class="control-label">Time</label>';?>
<?php echo TimePicker::widget(['model' => $model, 'attribute' => 'time']);?>

    <?php
    $service=Service::find()->all();
    $listData=ArrayHelper::map($service,'id','title');
    echo $form->field($model, 'serviceid')->dropDownList(
	                                $listData,
	                                ['prompt'=>'Select...']);?>
    
    <?php
    $barber=Barber::find()->all();
    $listData=ArrayHelper::map($barber,'id','name');
    echo $form->field($model, 'barberid')->dropDownList(

	                                $listData,

	                                ['prompt'=>'Select...']);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Book' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
	</div>
</div>
</div>
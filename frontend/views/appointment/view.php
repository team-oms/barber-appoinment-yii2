<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Appointment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-view">

    <h3>Appointment Number:<?= Html::encode($this->title) ?></h3>

    <p> <?php if(Yii::$app->user->can('manage')): ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            #'id',
            'date',
            'time',
            [                     
            'label' => 'Service Name',
            'value' => $model->service->title,
        ],
            [                     
            'label' => 'Customer Name',
            'value' => $model->customer->name,
        ],
            [                     
            'label' => 'Barber Name',
            'value' => $model->barber->name,
        ],
        ],
    ]) ?>

</div>

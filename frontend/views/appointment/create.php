<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Appointment */

$this->title = 'Book Appointment';
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<fieldset class="appointment-create">

    <legend><?= Html::encode($this->title) ?></legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</fieldset>

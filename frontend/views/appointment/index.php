<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppointmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appointments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php if(!Yii::$app->user->can('manage')): ?>
    <p>
        <?= Html::a('Book Appointment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php endif;?>
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            #'id',
            'date',
            'time',
            ['attribute'=>'service.title',
             'label'=>'Service',],
            ['attribute'=>'customer.name',
             'label'=>'Customer Name',],
            ['attribute'=>'barber.name',
             'label'=>'Barber Name',],

            ['class' => 'yii\grid\ActionColumn',
             #'template'=>'{view}',
             'template' => '{delete}{M}',
            ],
        ],
    ]);?>
    
   


</div>
